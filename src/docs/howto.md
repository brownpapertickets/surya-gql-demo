
# Brown Paper Tickets Surya-GQL Documentation
=======
# About Surya-GQL
Brown Paper Tickets technology team has selected GraphQL as our interface standard for our programmatic APIs. Read more below for exactly *how* we use it.
The framework has been distilled into a set of NPM modules called surya-gql
Aside: Surya is the Hindu sun god and is a close analog to Apollo the Helenistic sun god after which the Apollo-graphql system is named. Surya-gql uses Apollo-graphql as its platform hence the linkage.


## About the GraphQL Standard
GraphQL was created by Facebook and use internally since 2012 and open sourced in 2015. Since then a substantial community has developed around it, which is a key prerequisite for it to be a good option for adoption.  As such, please use the Google liberally for general reference information.  Here are a few recommended resources for general information.

* [Canonical Intro to GraphQL:](https://graphql.org/learn/)
* [Pro/Con analysis of GraphQL:](https://www.robinwieruch.de/why-graphql-advantages-disadvantages-alternatives/)

### GraphQL Benefits that we value

* Reduces "surface area" of API by enabling the client code to select the desired data
* Empowers the client to drill-through and get related data in a single call thus reducing API "chatter" and improving performance
* Strictly validates structure of request calls and response data. Less ad-hoc data transfers.
* provides self-documentation through built-in GraphQL "Playground" and api documentation.

## Apollo GraphQL
GraphQL is a standard and there are generic JS libraries that support it. However to make it easier to use 3rd parties have implemented additional high level node modules to support creating GraphQL servers.  We have
Apollo GraphQL is a NodeJS implementation of the GraphQL standard. Read the docs [here](https://www.apollographql.com/docs/)

## Surya-GQL
Surya-gql (just "Surya" for short) adds structure to help with scaling an application up to enterprise complexity and provides out-of-the box CRUD standard behavior on no-SQL and SQL back-end databases (Currently MongoDB and PosgreSQL, but openly extensible).
Surya is opinionated around signatures of basic CRUD functionality and as such makes roughing out persistent services faster, simpler, more reliable with with lots of powerful functionality built in.


### Accessing the API

- Development
If you are running the development BPT API service, you can access the GraphQL api endpoints at:

[http://localhost:3001/graphql](http://localhost:3001/graphql)

Pointing your browser to this endpoint in Development will open the GraphiQL "Playground" which allows you to exercise the endpoints interactively and see the introspectively generated documentation

- Production
In production the endpoint for the GraphQL API is
[https://api.bptnextgen.com/v3/graphql](https://api.bptnextgen.com/v3/graphql)
The interactive console is *not* available, for security purposes by default.  You can point your dev Playground to this endpoint but be aware that it will 400 after you get a successful result because it is pinging the server for documentation information which is disabled, so your results will quickly disappear.

### Surya Platform Architecture

#### Types
Surya is built around a strong concept of Type which defines an important, often (but not always) persisted data object. The Type object may be complex and have
other subsidiary sub-objects or share common object structures.  This matches well the the concept of a MongoDB Collection, so if we have a MongoDB Collection, there will be a corresponding Type module. If the Type is backed by PostgreSQL then a type will probably be the manifestation of a single Table. 

A complete Surya Type module will define the object structure in a universal definition that will generate the GraphQL Schema and a database persistence Schema such a Mongoose (or other database as appropriate). It also defines, security constraints and data valididations in a declarative manner.
The Type module also expresses the functional methods of the object and the GraphQL interface in standard GraphQL language.
The framework provides for standardized implementation of CRUD+ logic at the API and the data persistence layers. These can be extended as appropriate, can be called between Types in a safe and consistent manner. Types that do not match up with the standard CRUD pattern can use lower level base implementations to build alternative patterns including calling MQ-linked services or calling remote internal or external REST services.

#### Components of the Surya Framework

* Express : Baseline Web Application Framework

* ApolloGraphQL : Plugs into Express and adds the /graphql endpoint processing

* Custom Type Components
 * Type definition : A "universal" definition of the object structure, validation rules and security/access rules.  The Base classes of the type system contain the logic for trans-compilation to GraphQL Schema at Service start-up.  The logic generates output, input and standardized Mutation response schema formats, which also serves to improve standardization of the API across Types.
 * <Type>Manager : A Class that encapsulates the dynamic business methods associated with the type.  This is called by the Apollo Resolver when a registered Query or Mutation is recognized.  The Manager contains no direct persistence logic but is a place where business rules that cannot be declaratively described can be implemented. A set of base class implementations helps to reduce code and encourage standard logic.  The base classes of the Manager system typically implement security as driven by the Univeral Type definition above.

 * GraphQL Query and Mutation definitions : These are essentially standard GraphQL language definitions of the GraphQL API.  When writing the Queries and Mutations, knowledge of the Schema generation standards of the Type system is important.  Breaking out of this standard should only be done with discussion and should probably by done by extending the Type system auto-generation logic.
 * Resolver : Resolvers are a part of the Apollo/GraphQL implementation framework. The are simple functions called when a Query or Mutation described in the previous point are recognized by the Apollo framework.  In the Surya Framework, these are very slim shims that immediately dispatch to the associated Manager and perform only a routing function. 


### Surya Type modeling

The Surya Framework features a "universal" Type definition language which declaratively describes the structure and basic business characteristics of API and persistence data.  The Type definition is implemented as Class named for the type in "PascalCase".  For example for a hypothetical Type named "Animal", it would be a file at

```
/src/actor/api/graphql/types/animal/animal.js
```

An example for the file would be...

```
import { BaseType, Permissions } from "../baseType"
import { ScalarTypes } from "../../scalars/scalars"

export class Animal extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "Animal",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        id: {
          type: ScalarTypes.ID,
          key: true,
          required: true,
          input: false,
        },
        name: {
          type: ScalarTypes.String,
          required: true,
          index: true,
          unique: true,
          readPermission: Permissions.Public,
        },
        owner: {
          type: ScalarTypes.ObjectRef,
          objectType: "User",
          refField: "ownerId",
          refFieldType: ScalarTypes.ID,
          ownerId: true,
        },
        keywords: {
          type: ScalarTypes.String,
          array: true,
        },
        weightPounds: {
          type: ScalarTypes.Float,
          readPermission: Permissions.Personal,
        },
        gender: {
          type: ScalarType.String,
          readPermission: Permissions.Secure,
        },
        address: {
          type: ScalarTypes.Object,
          objectType: "Address",
        },
        createdDate: {
          type: ScalarTypes.DateTime,
        },
        updatedDate: {
          type: ScalarTypes.DateTime,
        },
        alive: {
          type: ScalarTypes.Boolean,
          readPermission: Permissions.Personal
      }
    }
  }
}

```

#### Type global properties
| Property        | Required    | Definition                                     |
| --------------- |-------------| -----------------------------------------------|
| name  | required | The name of the Type. Should be the same as the Class name
| instantiable |  | True: The Type is expected to be persisted as a primary object with an ID that can be retrieved. Must be *true* for Graphql generation to produce [Type]SetRespose and [Type]MutationRespose data definitions
| timestamps |  | If True: fields "createdDate" and "updatedDate" will be automatically generated and maintained by Mongoose system.  Note, you must define the fields explicity in the fields list in order for them to be present in the API and accessible.
| permissions |  | Contains an object/map that defines the groups that have certain permission types for the object.  The one currently being used and strongly suggested is "Secure".  The value is a list of group names.  The pseudo-group "admin" corresponds to users with property "administrator": true.  This allows for extension of a more elaborated user/group model for permissions management.
| inputs | | a list of 1 or more additional input types you want to be generated for the type. This is specifically intended to allow referenced sub-objects to be included inline to support aggregate mutations. Important:  Choose a name that is not already generated by the system.  See field properties below. This name must match in the inputs: field value for refObjects you want to be included in this aggregate input object.


#### Fields properties
Following the global Type properties is a Fields object/map that defines the fields present for the Type.

| Property        | Required    | Definition                                     |
| --------------- |-------------| -----------------------------------------------|
| type	| required | Type of the field. This is interpreted by the transcoding logic for GraphQL, MongoDB/Mongoose respectively or for some other persistence/database TBD. See table below for details.
| required |  | If True: the field must be present for all GraphQl calls and is non-nullable/required in a data store (e.g. Mongodb)
| key | | If True: Is understood to be the Id of the underlying DB store.  As such for MongoDb will have the persistence field name of _id, will be and ObjectId, required and auto-generated GUID.
| input || If False: the field will not be present in Input GraphQL structures. Is primarily useful for hiding the Id field from Input (create) data structures.  If it is desired to allow the client to specify the ID of a record, leave it off or set to: true.
| readPermission |  | Defaults to Permissions.Public. Permissions possible are: Public (all users may request and get this field; Personal:  The owner of the object (if applicable) may read the field.  Secure: If the calling user is in a group with Secure permissions, they may see the field value.  Any user may request the field but the value will be NULL if the permission test does not pass.  This applies for "get" and "query" type operations. Create and Update operations do not perform read filtering of responses.  See Manager security for control of which actions can be taken by a user.
| array || if True, the field is an array of whatever type is specified.
| ownerId || If True, the field contains the id that matches the user id of the owner.  This is used by default authorization logic to enact the "personal" field read and access permissions.  If ownership is determined by more complex rules (e.g. transitive ownerhip via a relationship), the logic may be overriden by the isOwner() method on the Type object.
| objectType || If the type: value is ScalarTypes.Object or ScalarTypes.ObjectRef, the value should be the name of the Type that is passed, returned and stored for this field. This works well for an object data store such as MongoDB or some of other JSON store. If the underlying DB is Relational, magic will happen and this is an implicit 1-M or M-M relationship declaration.
| refField || If type: is ScalarTypes.ObjectRef, this property must be present and specifies the underling name of the field that contains the id of the related object. This field will be the join field name in the database and will be the field name in the Input[Type] GraphQL schema.
| refFieldType || if type: is ScalarTypes.ObjectRef this is the type of the refField.  Pretty much always: ScalarTypes.ID
| inputs || This is only useful for fields of type ObjectRef.  Normally input schemas will represent an ObjectRef field as the refField name.  this field may be a list of 1 or more input schema names to be generated that match with an inputs: field name value in the header.  If so the schema output will include an input schema reference as if it were an embedded Object type. This allows an aggregate mutation to get sub-object data.


#### Field Types
The Unified Type system provides a standard mappable set of types that transform to API and data storage objects and are accessible at the Manager level for business logic.
Other custom scalars may be added as needed.

| Type                  | Description                             | GraphQL Schema        | MongoDb/Mongoose Schema |
|-----------------------|-----------------------------------------|-----------------------|-------------------------|
| ScalarTypes.String    | A string of any size	 			      | String			      | String
| ScalarTypes.ID	    | A (GUID) Identifier                     | ID (String)		      | ObjectId (UUID)
| ScalarTypes.DateTime  | A GMT-based ISO formatted Date          | Date (custom Scalar)  | Date
| ScalarTypes.Integer   | An Integer number (Unspecified Size)	  | Int					  | Number
| ScalarTypes.Float     | A Number with decimal extension		  | Float                 | Number
| ScalarTypes.Boolean   | A True/False value                      | Boolean	              | Boolean
| ScalarTypes.Object    | A contained sub-structure (ref by name) | [Type]                | [Type]
| ScalarTypes.ObjectRef | A referenced Type joined by Id field    | [Type]                | Foreign-Id join field (ID)

### Writing GraphQL Query/Mutation defintions
GraphQL API definitions are written in a standard-defined language as described [here](https://graphql.org/learn/queries/).  Surya currently focuses on a subset of possible GraphQL Query and Mutation features and we can expand support as needed. Furthermore we highly encourage a standardized pattern of naming conventions and parameters across Type Modules as described below.

Here is an example from the User Type module:

```
import { gql } from "apollo-server"

export const UserQueries = gql`
  extend type Query {
    users(
      offset: Int
      limit: Int
      filter: String
      sort: String
    ): UserSetResponse
    user(id: ID!): User
  }

  extend type Mutation {
    createUser(user: UserInput!): UserMutationResponse!
    updateUser(id: ID!, user: UserInput!): UserMutationResponse!
    deleteUser(id: ID!): UserMutationResponse!
  }
```
---

**Important:** Because the Surya TypesManager aggregates the Queries and Mutations of each Type module together, as shown in the example above, it is *essential* that the modifier '__extend__' be used before the type Query and type Mutation definitions.

---

The User module is a very standard CRUD Type at this time and the Query and Mutation definitions follow the current Surya standard.

Query and Mutation definitions use camel-case
* Get : named lower case singular of type with argument: (id: ID!) returning an instance of Type

* Query: named lower case plural form of type with arguements:
	  offset: Int
      limit: Int
      filter: String
      sort: String

   returning [Type]SetResponse type.

* Create : named create[Type] with argument ( [type]: [Type]Input! ): returning [Type]MutationResponse!

* Update : named update[Type] with arguments (id: ID!, [type]: [Type]Input!): returning: [Type]MutationResponse!

* Delete : named delete[Type] with arguments (id: ID!): returning [Type]MutationResponse!

#### Standard Surya Type to GraphQL Schema productions
Given a Type named "Foo" that is "instantiable: true", the following GraphQL Schema types will be output:

| Schema Type         | Description
|---------------------| ---------------
| Foo                 | The full object returned by Get, Query, Create, Update or Delete operations
| FooInput            | An Input type used when creating new Foo objects. FooInput is like Foo except that fields with input:false will not be present and fields of type ObjectRef will be present with the name specified for *refField*.
| FooSetResponse      | A Type returned by a query or other set response query.  This type follows the pattern
| FooMutationResponse | A Standardized Mutation response for a single mutation operation (Create/Update/Delete)

##### FooSetResponse Template
```
type FooSetResponse {
	items: [Foo]!
	total: Int
	count: Int
	offset: Int
}
```

##### FooMutationResponse Template
```
type FooMutationResponse {
	success: Boolean!
	message: String
	data: Foo
}
```

Adherance to these standards helps to leverage the standard Manager classes and provides for predictability for API users.  If different response patterns are required in single cases, the developer can define the needed structures explicitly as part of the type.js file.

---

***Note*** See the **Shipment** Type module as an example of a non-CRUD, non-standard pattern where response types are explicitly defined.

---


### Writing Resolvers
For Surya, Resolvers should simply be slim dispatches to the appropriate Manager method.  Resolvers are simple stateless functions that are called when the GraphQL front end recognizes a Query or Mutation defined as above. Here we use the example of the CRUD patterned Event Type module.

Resolver functions are all async and receive three arguments:
1: the Object, used when resolving referenced fields, and not generally used for Query or Mutation calls
2: arguments object with args named as per the Query/Mutation definitions above
3: context object which currently contains two important properties: (auth- the authorization token to be used by the manager to do authentication and authorization) (managers - a meta-manager object that contains a map of all managers to recall by Type name).

If the Type definition has ObjectRef fields, the Resolver should include fetchers that match the name of the fields and use the complementary Manager objects .getLinked() method to fetch the object by id.

the third argument of most manager methods is an Options object which usually has a doAuth property.  When called directly from the resolvers, it is highly encourage to specify that authorization be performed by the manager according to the Type model/manager specifications.  When calling from Manager to Manager, it may be valuable to elevate permissions to override the ability normally associated with the calling use.  As such it is possible to call with doAuth: false, when it is called for as long as the data values are not returned to the user and used only for business logic.

```
export const EventResolvers = {
  Query: {
    events: async (_, args, context) => {
      return await context.managers
        .get("Event")
        .query(args, context, { doAuth: true })
    },
    event: async (_, args, context) => {
      return await context.managers
        .get("Event")
        .get(args, context, { doAuth: true })
    },
  },
  Mutation: {
    createEvent: async (_, args, context) => {
      return await context.managers
        .get("Event")
        .create(args.event, context, { doAuth: true })
    },
    updateEvent: async (_, args, context) => {
      return await context.managers
        .get("Event")
        .update(args.id, args.event, context, {
          doAuth: true,
        })
    },
    deleteEvent: async (_, args, context) => {
      return await context.managers
        .get("Event")
        .delete(args.id, context, { doAuth: true })
    },
  },
  Event: {
    venue: async (obj, __, context) => {
      return await context.managers
        .get("Venue")
        .getLinked({ obj: obj, byField: "venueId" }, context, { doAuth: true })
    },
    owner: async (obj, __, context) => {
      return await context.managers
        .get("User")
        .getLinked({ obj: obj, byField: "ownerId" }, context, { doAuth: true })
    },
  },
}

```


### Surya Type Managers

For most Surya Types, the Manager is where the bulk of the code and business logic will reside.  A set of standard base classes will provide the essential underpinnings for your manager and could substantially reduce the amount of code that must be written, particularly if your Type follows the CRUD pattern.

Typical Call sequence
```
HTTP Query => Apollo/GraphQL => TypeResolver => TypeManager => DataInterfaceAPI <=> MongoDB or other data store
                  ^                                 V
                  |                             BaseManagers
                  |                                 V
                  +--- Type Query                   +-- Auth/Security

```


Type Managers should inherit from BaseCRUDManager if they conform to a CRUD pattern.  CRUD methods can be overridden as shown to perform pre/post or replace functionality.

---

**Important!**:   If replacing a CRUD method, the new method should implement authorization and, as appropriate ReadSecurity calls following the base class patterns.

---


```
import { BaseCRUDManager } from "../baseCRUDManager"

const authorized = {
  get: { groups: ["*"] },
  query: { groups: ["*"] },
  create: { groups: ["*"] },
  update: { groups: ["admin"], owner: true },
  delete: { groups: ["admin"] },
}

export class EventManager extends BaseCRUDManager {
  authorized() {
    return authorized
  }

  async create(data, context, options) {
    data.postedDate = new Date().toISOString()
    this.log.info(`Create Event ${JSON.stringify(data, null, 2)}`)
    return super.create(data, context, options)
  }
}
```

The authorized() function should return an object defining the security capability tokens and the groups that are permitted to perform those operations.
Currently groups are: *:everyone, admin: users with administrator: true.
If owner: true for security capbility, then if the calling use is the owner of the object in question, they will be allowed to perform the operation even if they are not in the authorized groups.

### Surya Data APIs

If a Type Module is to be backed by a MongoDB Collection or a SQL Table, it is usually quite simple to create the API based on the BaseMongoAPI or BasPGSqlAPI Class.  The Base Mongo API gets it's information from the Type which is passed to it at instantiation when the server is re/started.  The Collection will be created if it does not exist and generally additive changes to the schema will be tolerated
The BasePGSqlAPI does not creat or alter tables when the Type definition is changed.  The Type and Table must be kept in sync manually.

TBD: Breaking Schema changes to Mongodb/Mongoose schema collections.

Example vanilla API based on BaseMongoAPI.

```
import BaseMongoAPI from "./BaseMongoAPI"

export class UsersAPI extends BaseMongoAPI {
  constructor(container, type) {
    super(container, type)
  }

  getCollection() {
    return this.db.collections["User"]
  }

  getTypeName() {
    return "User"
  }
}


```


## Surya query language
To provide ease of use and consistency, the Surya framework includes a standardized filter  and sort query language which is very similar to the SQL WHERE syntax and SQL ORDER BY syntax and semantics respectively. If other data back ends such as SQL or other no-sql data stores are used, the same query language will be used as the interface so developers can rely on a consistent API.  GraphQL does not specify  such a system and usually relies on narrowly focused parameter passing which we believe dilutes the client-empowered and small-surface-area promise of GraphQL. The flexibity offered by such an open system creates certain security and performance challenges however which we will need to address over time.

Because the language is fully and comprehensively parsed via a recursive descent grammar/parser and particularly for MongoDB, injection attacks are not a concern.  We do provide for field level read permissions so many of these concerns should be addressed.

### Filter language
The filter language is parsed for a string passed to the "filter" argument of query methods.  This parser may be used for other query or mutation calls if appropriate.  For example a bulk update or delete or other mutation operation could be written that would use the filter parsing logic.

Predicates are the individual conditions that must be satisfied for an object to be returned as a query result.

Examples:

- firstName = 'John'
- ticketCount >= 1000
- address.state IN ( 'WA', 'OR', 'CA', 'AK' )
- emailToken EXISTS
- email MATCHES '@brownpapertickets.com$'

Predicates generally follow the form of

[FieldName] [ Operator ] [LiteralValue ]

---
**Note** For MongoDb backed Types, predicates can reference sub-fields (fields of type Object), but not sub-fields of ObjectRef type fields at this time.

---

So Venue.address.city is fair game because it's stored in the same Document but Event.vendor.name is not because it is stored in a separate Document linked by vendorId. Maybe magic will happen in the future to allow this.

### Literals

String literals are contained in single quotes only ( e.g. 'this is a string') Not: ( "No double quotes", `no grave accents` ).
Note: currently there is no escaping for single quotes. TODO:

Numbers can be integers (e.g. 1024) or floating point (123.456)
Numbers may be negative indicated by a preceding '-' with no space before the number: (e.g. -55.0001 )

Dates: are ISO formatted date strings enclosed in single quotes

Boolean: true, false are allowed boolean values equivalient to 1 and 0.

ID guids can be formatted as string representations.

Exceptions:
Exists and !Exists predicates are unary and have not value
IN and !IN predicates have a comma delimited list of string, numeric or boolean literal surrounded by parentheses.

#### Predicate Operators

| Operator        | Name                 | Description                                                     | Example                 |
|-----------------|----------------------| ----------------------------------------------------------------|-------------------------
| =               | Equals               | Case Sensitive string, numeric, date, boolean exact match       | status = 'Bogus'
| !=              | NotEquals            | True if field does not exactly match value                      | lastName != 'Anderson'
| >               | GreaterThan          | True if value is greater than or equal to, for string or number | value > 98.99         |
| >=              | GreaterThanOrEqualTo | True if value is greater than or equal to                       | activeDate >= '2019-05-01'
| <               | LessThan             | True if field value is less than                                | ticketCount < 50
| <=              | LessThanOrEqualTo    | True if field value is less than or equal to                    | foo <= 'stuff'
| MATCHES		  | Matches              | MATCHES or matches (upper or lower case) REGEX pattern matching | name MATCHES '^[a-zA-Z][a-zA-Z0-9\._]{0,20}'
| EXISTS          | Exists               | The field exists (useful for semi-structured db like MongoDb    | emailToken EXISTS
| IN              | ValueInList          | Value is in a list | address.state IN ( 'OK', 'TX', 'MT' )
| !IN             | ValueNotInList       | Value not in list | count !in ( 1, 3, 5, 6.7, false )


#### Boolean operators

* AND (AND, and)   Ands evaluate before ors
* OR (OR, or )

Examples:

Example1:
```
firstName = 'John' AND lastName = 'Lyon-Smith' OR firstName = 'Ben' AND lastName = 'Goldstein'
```
The AND clauses are evaluated first and then OR'd so this finds all (Users) named "John Lyon-Smith" or "Ben Goldstein"

Example2:
```
lastName = 'Harder' OR lastName = 'Boylan' AND firstName = 'Erika' OR firstName = 'Laura'
```
This finds all uses with last name "Harder" OR first name Laura" OR users named "Erika Boylan".

#### Precedent forcing with parenthesis.
Evaluation order can be forced by enclosing clauses in parentheses.

Example3:
```
( lastName = 'Harder' OR lastName = 'Boylan' ) AND ( firstName = 'Erika' OR firstName = 'Laura' )
```
This query finds all users named 'Erika Harder' OR 'Erika Boylan' OR 'Laura Harder' OR 'Laura Boylan'.

#### PGSQL Automatic joins and indexed joins
TODO:

#### Filtering By Symbolics
TODO:

### Sort language

The sort language is very simple by comparison.  The sort specification is a field name or comma delimited list of fields with an optional ASC (Ascending) or DESC (Descending) specific with Ascending being the default.

Example:

* createdDate // by createdDate ascending
* createdDate DESC // by createdDate descending
* userName // by userName ascending (note sort order on strings is case sensitive at this point.
* country, state, city DESC // by country, by state, by city descending.


## Add a New Type Module in the {BGM} Framework
For details on writing Type definitions, Queries, Mutations Resolvers, Managers etc. See some of the documentation above and review the existing Type Modules as examples.  This section pulls together all the things you need to do in order and some of the *glue* pieces that make it come together.  For this example we will assume you are creating a straight-forward CRUD Type Module that uses MongoDb as the persistence storage.

For this example we will create a Type module for a simple Ticket model.

### Type Module directory
The artifacts of your new Type Module will reside an a directory under
```
/src/actor/api/graphql/types
```
Name your module directory for your type in camel-case

```
.../graphql/types/ticket
```

### Write Your Type Definition
In your module directory first create a file named [type].js which will contain your Type class definition.

```
// ticket.js
import { BaseType, Permissions } from "surya-gql-types"
import { ScalarTypes } from "surya-gql-scalar"

export class Ticket extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "Ticket",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        id: {
          type: ScalarTypes.ID,
          key: true,
          required: true,
          input: false,
        },
        owner: {
          type: ScalarTypes.ObjectRef,
          objectType: "User",
          refField: "ownerId",
          refFieldType: ScalarTypes.ID,
          ownerId: true,
          required: true,
          readPermission: Permissions.Personal,
        },
        event: {
          type: ScalarTypes.ObjectRef,
          objectType: "Event",
          refField: "eventId",
          refFieldType: ScalarTypes.ID,
          required: true,
        },
        createdDate: {
          type: ScalarTypes.DateTime,
          input: false,
        },
        updatedDate: {
          type: ScalarTypes.DateTime,
          input: false,
        },
        seat: {
          type: ScalarTypes.Object,
          objectType: "TicketSeat",
        },
        purchasePrice: {
          type: ScalarTypes.Float,
        },
        purchaseComplete: {
          type: ScalarTypes.Boolean,
        },
        notes: {
          type: ScalarTypes.String,
          readPermission: Permissions.Secure,
        },
      },
    }
  }
}

export class TicketSeat extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "TicketSeat",
      extends: null,
      instantiable: false,
      timestamps: false,
      fields: {
        isGeneralAdmission: {
          type: ScalarTypes.Boolean,
        },
        section: {
          type: ScalarTypes.String,
        },
        row: {
          type: ScalarTypes.String,
        },
        seat: {
          type: ScalarTypes.String,
        },
        adaAccess: {
          type: ScalarTypes.Boolean,
        },
      },
    }
  }
}


```

### Write your GraphQL Query/Mutation definitions

```
// ticketQueries.js
import { gql } from "apollo-server"

export const TicketQueries = gql`
  extend type Query {
    tickets(
      offset: Int
      limit: Int
      filter: String
      sort: String
    ): TicketSetResponse
    ticket(id: ID!): Ticket
  }

  extend type Mutation {
    createTicket(ticket: TicketInput!): TicketMutationResponse!
    updateTicket(id: ID!, ticket: TicketInput!): TicketMutationResponse!
    deleteTicket(id: ID!): TicketMutationResponse!
  }
`
```
### Write Resolvers

```
// ticketResolvers.js
// standard CRUD api resolvers
// includes resolvers for objectRefs event, owner, cart

export const TicketResolvers = {
  Query: {
    tickets: async (_, args, context) => {
      return await context.managers
        .get("Ticket")
        .query(args, context, { doAuth: true })
    },
    ticket: async (_, args, context) => {
      return await context.managers
        .get("Ticket")
        .get(args, context, { doAuth: true })
    },
  },
  Mutation: {
    createTicket: async (_, args, context) => {
      return await context.managers
        .get("Ticket")
        .create(args.ticket, context, { doAuth: true })
    },
    updateTicket: async (_, args, context) => {
      return await context.managers
        .get("Ticket")
        .update(args.id, args.ticket, context, {
          doAuth: true,
        })
    },
    deleteTicket: async (_, args, context) => {
      return await context.managers
        .get("Ticket")
        .delete(args.id, context, { doAuth: true })
    },
  },
  Ticket: {
    event: async (obj, __, context) => {
      return await context.managers
        .get("Event")
        .getLinked({ obj: obj, byField: "eventId" }, context, { doAuth: true })
    },
    owner: async (obj, __, context) => {
      return await context.managers
        .get("User")
        .getLinked({ obj: obj, byField: "ownerId" }, context, { doAuth: true })
    },
  },
}

```
### Write Manager
This is a completely generic CRUD manager. Only auth needs to be specified.

```
// ticketManager.js
import { BaseCRUDManager } from "surya-gql-types"

// any authenticated user can get, query or create a Ticket. But only the owner or an Admin can update one. And only Admins can delete them.
const authorized = {
  get: { groups: ["*"] },
  query: { groups: ["*"] },
  create: { groups: ["*"] },
  update: { groups: ["admin"], owner: true },
  delete: { groups: ["admin"] },
}

export class TicketManager extends BaseCRUDManager {
  authorized() {
    return authorized
  }
}
```


### Add Data Interface/API
We will create a standard CRUD MongoDB API by subclassing the highly capable base class.
Only the getCollection() and getTypeName() methods must be overridden to get the baseline CRUD functionality.


```
// ticketsAPI.js

import { BasePGSqlAPI } from "surya-gql-data-pgsql"

export class TicketsAPI extends BasePGSqlAPI {
  constructor(container, type, typeMap, config) {
    super(container, type, typeMap, config)
  }
}

```


### index.js manifold
Now add an index.js file in your Type module directory to pull all your type asset exports into one convenient package:

```
// index.js
export { Manifest } from "./manifest" 
export { Ticket, TicketSeat } from "./ticket" // all types exported
export { TicketManager } from "./ticketManager"
export { TicketQueries } from "./ticketQueries"
export { TicketResolvers } from "./ticketResolvers"
export { TicketAPI } from "./ticketAPI"

```

### Write the Manifest

The Surya TypesManager.loadTypeModules method scans each directory of the specified root that contains a "manifiest.js" file.
The manifest which looks something like this:
```
export const Manifest = {
  sysVersion: "1.0.0",
  typeVersion: "0.1.0",
  description: "MongoDB Based Users storage",
  types: ["User", "Address", "AuthToken"],
  queries: "UserQueries",
  resolvers: ["UserResolvers"],
  manager: "UserManager",
  data: {
    interface: "MongoDB",
    api: "UserAPI",
    config: { collection: "demo-user" },
  },
}
```

Provides information to the loader to help it understand the role of each the files in the module. A naming convention is also available so that a placeholder manifest can be used for routine structured modules.

### Types: A list of the names of classes that are derived from BaseType and may be refrenced by queries or mutations.
Note: for each defined type name. For example "User", the BaseType class will generate a set of related data structures automatically for registration with the GraphQL system:
<CLASS>		 : The class as defined
<CLASS>Input : For use as input structures for example for create and update calls. This structure will not include fields flagged as input: false or symbolic: true
<CLASS>SetResponse:  A response from a call that returns a list of <CLASS> objects as:
	success: boolean	: true if the query succeeded
	offset: integer		: the passed offset
	total: integer		: Total number of objects returned by query before pagination.
	items: [ <CLASS> ]   : Array of <CLASS> objects

<CLASS>MutationResponse: Returned by mutation operations create, update, delete, etc.
success: boolean
message: string
data: <CLASS>

Details on writing Type classes are detailed <here>

### Queries: A straight-forward representation of the graphql specification for the queries, mutations and subscriptions for these types

### Resolvers:  A resolver is a map of simple functions that are called when a query, mutation or subscription is called by the client.
The function is passed arguments, possibly the relevant data object and a *context* object which you define when you intantiate the ApolloGraphQL server.  By convention for Surya, it contains a logger, the map of Managers for all of the Type modules keyed by the TypeName and the Authorization token or credentials
The "Surya Way" is to minimize code in the resolvers and look up the appropriate manager and call it's corresponding method passing through the arguments, context and options.  Some minor logic may be involved for individual field resolvers.
Thought there is usually a single resolver file, for more complex cases, multilple resolver files may be specified.
Note:  The demo should add some resolver examples for single and multiple lookups and for symbolics.

### Managers: Standardized classes that do the business logic heavy lifting for Type Modules.
Managers are built from BaseClasses that provide basic functionality like authorization and authentication and possibly standardized CRUD logic.
Methods can be added to managers where needed or standard methods can be sub-classed for replacement or for pre and/or post operations to the standard logic.
For most straight-forward cases where CRUD+ logic is called for, only a placeholder is needed as all of the basic functionality is built in.

### Data APIs
If your Type Module is a standard CRUD data interface you can define your API in the data: section.
interface: defines what kind of database or data store you are using.
api: is the class you defined, which is typically subclassed from a standard API base class
config: is an API-specific configuration data object.  See the BaseAPI in each case.

#### Mongo Configration options
Mongo supports the "collection" config option to specify the name of the underlying collection

```
data: {
    interface: "MongoDB",
    api: "AccountAPI",
    config: { collection: "accounts" },
  },
```
#### PgSQL Configuration options
PgSQL supports the "table" and "getTotal" config options

* table -- the backing table name
* getTotal -- when executing queries, should the total results before pagination be returned.  Defaults to True.

```
  data: {
    interface: "PGSql",
    api: "EmailQueueAPI",
    config: { table: "mails_2020", getTotal: false },
  },
```


