import { gql } from "apollo-server"

export const CommonGQL = gql`
  scalar DemoScalar
  scalar EmailScalar

  enum ENUM_RGB {
    RED
    GREEN
    BLUE
  }

  enum ENUM_GENDER {
    TRANSGENDER
    NON_BINARY
    OTHER
    UNKNOWN
    FEMALE
    MALE
  }

  enum ENUM_COUNTRIES {
    USA
    CANADA
    MEXICO
    UNITED_KINGDOM
    GERMANY
    FRANCE
    SPAIN
    INDIA
    CHINA
  }
`
