export const Manifest = {
  sysVersion: "1.0.0",
  typeVersion: "0.1.0",
  description: "Common Classes, Enums and Scalars",
  types: ["Address"],
  queries: "CommonGQL",
  resolvers: ["EnumResolvers", "DemoScalar", "EmailScalar"],
}
