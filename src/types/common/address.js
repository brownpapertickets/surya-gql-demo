import { BaseType, Permissions } from "@brownpapertickets/surya-gql-types"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"

export class Address extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "Address",
      extends: null,
      instantiable: false,
      fields: {
        address1: {
          type: ScalarTypes.String,
        },
        address2: {
          type: ScalarTypes.String,
        },
        city: {
          type: ScalarTypes.String,
        },
        state: {
          type: ScalarTypes.String,
        },
        postalCode: {
          type: ScalarTypes.String,
        },
        country: {
          type: ScalarTypes.Enum,
          name: "ENUM_COUNTRIES",
        },
      },
    }
  }
}
