import { BaseType, Permissions } from "@brownpapertickets/surya-gql-types"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"

export class Artist extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "Artist",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        id: {
          type: ScalarTypes.ID,
          key: true,
          required: true,
          input: false,
        },
        name: {
          type: ScalarTypes.String,
          index: true,
          unique: true,
        },
        sortName: {
          type: ScalarTypes.String,
        },
        type: {
          type: ScalarTypes.Enum,
          values: [
            "PERSON",
            "GROUP",
            "ORCHESTRA",
            "CHOIR",
            "CHARACTER",
            "OTHER",
          ],
        },
        gender: {
          type: ScalarTypes.Enum,
          name: "ENUM_GENDER",
        },
        beginDate: {
          type: ScalarTypes.DateTime,
        },
        endDate: {
          type: ScalarTypes.DateTime,
        },
        ipiCode: {
          type: ScalarTypes.String,
        },
        isniCode: {
          type: ScalarTypes.String,
        },
        aliases: {
          type: ScalarTypes.String,
          array: true,
        },
        disambiguation: {
          type: ScalarTypes.String,
        },
        annotation: {
          type: ScalarTypes.String,
        },
        owner: {
          type: ScalarTypes.ObjectRef,
          objectType: "User",
          refField: "ownerId",
          refFieldType: ScalarTypes.ID,
        },
        createdDate: {
          type: ScalarTypes.DateTime,
        },
        updatedDate: {
          type: ScalarTypes.DateTime,
        },
      },
    }
  }
}
