import { gql } from "apollo-server"

export const ArtistQueries = gql`
  extend type Query {
    artists(
      offset: Int
      limit: Int
      filter: String
      sort: String
    ): ArtistSetResponse
    artist(id: ID!): Artist
  }

  extend type Mutation {
    createArtist(artist: ArtistInput!): ArtistMutationResponse!
    updateArtist(id: ID!, artist: ArtistInput!): ArtistMutationResponse!
    deleteArtist(id: ID!): ArtistMutationResponse!
  }
`
