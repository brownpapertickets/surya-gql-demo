export const Manifest = {
  sysVersion: "1.0.0",
  typeVersion: "0.1.0",
  description: "MongoDB Based Artists storage",
  types: ["Artist"],
  queries: "ArtistQueries",
  resolvers: "ArtistResolvers",
  manager: "ArtistManager",
  data: {
    interface: "MongoDB",
    api: "ArtistAPI",
    config: { collection: "music-artist" },
  },
}
