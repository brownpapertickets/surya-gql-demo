export const ArtistResolvers = {
  Query: {
    artists: async (_, args, context) => {
      return await context.managers
        .get("Artist")
        .query(args, context, { doAuth: true })
    },
    artist: async (_, args, context) => {
      return await context.managers
        .get("Artist")
        .get(args, context, { doAuth: true })
    },
  },
  Mutation: {
    createArtist: async (_, args, context) => {
      return await context.managers
        .get("Artist")
        .create(args.artist, context, { doAuth: true })
    },
    updateArtist: async (_, args, context) => {
      return await context.managers
        .get("Artist")
        .update(args.id, args.artist, context, {
          doAuth: true,
        })
    },
    deleteArtist: async (_, args, context) => {
      return await context.managers
        .get("Artist")
        .delete(args.id, context, { doAuth: true })
    },
  },
}
