import { BaseMongoAPI } from "@brownpapertickets/surya-gql-data-mongodb"

export class ArtistAPI extends BaseMongoAPI {
  constructor(container, type, typeMap, config) {
    super(container, type, typeMap, config)
    this.log.info(
      `ArtistAPI create type: ${this.getTypeName()} config:${JSON.stringify(
        config
      )}`
    )
  }
}
