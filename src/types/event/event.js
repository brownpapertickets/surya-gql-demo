import { BaseType, Permissions } from "@brownpapertickets/surya-gql-types"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"

export class Event extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "Event",
      extends: null,
      instantiable: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        id: {
          type: ScalarTypes.ID,
          asName: "e_id",
          key: true,
          required: true,
          input: false,
        },
        name: {
          type: ScalarTypes.String,
          asName: "e_name",
        },
        ownerId: {
          type: ScalarTypes.Integer,
          asName: "c_id",
        },
        postedDate: {
          type: ScalarTypes.DateTime,
          asName: "posted_date",
        },
        startDate: {
          type: ScalarTypes.DateTime,
          asName: "e_start_date",
        },
        description: {
          type: ScalarTypes.String,
          asName: "e_description",
        },
        sellTickets: {
          type: ScalarTypes.Boolean,
          asName: "e_sell_tickets",
        },
        siteEventFee: {
          type: ScalarTypes.Float,
          asName: "site_event_fee",
        },
        sitename: {
          type: ScalarTypes.String,
        },
        venue: {
          type: ScalarTypes.String,
          asName: "e_venue",
        },
        city: {
          type: ScalarTypes.String,
          asName: "e_city",
        },
        state: {
          type: ScalarTypes.String,
          asName: "e_state",
        },
        country: {
          type: ScalarTypes.String,
          asName: "e_country",
        },
        approved: {
          type: ScalarTypes.Boolean,
          asName: "e_approved",
        },
        approvedDate: {
          type: ScalarTypes.DateTime,
          asName: "approved_date",
        },
        approvedById: {
          type: ScalarTypes.Integer,
          asName: "approval_c_id",
        },
        complete: {
          type: ScalarTypes.Boolean,
          asName: "e_complete",
        },
        activated: {
          type: ScalarTypes.Boolean,
          asName: "e_activated",
        },
        defaultUserEvent: {
          type: ScalarTypes.Boolean,
          asName: "e_default_user_event",
        },
      },
    }
  }
}
