export const EventResolvers = {
  Query: {
    events: async (_, args, context) => {
      return await context.managers
        .get("Event")
        .query(args, context, { doAuth: true })
    },
    event: async (_, args, context) => {
      return await context.managers
        .get("Event")
        .get(args, context, { doAuth: true })
    },
  },
  Mutation: {
    createEvent: async (_, args, context) => {
      return await context.managers
        .get("Event")
        .create(args.event, context, { doAuth: true })
    },
    updateEvent: async (_, args, context) => {
      return await context.managers
        .get("Event")
        .update(args.id, args.event, context, {
          doAuth: true,
        })
    },
    deleteEvent: async (_, args, context) => {
      return await context.managers
        .get("Event")
        .delete(args.id, context, { doAuth: true })
    },
  },
}
