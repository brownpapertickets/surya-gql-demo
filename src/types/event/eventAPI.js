import { BasePGSqlAPI } from "@brownpapertickets/surya-gql-data-pgsql"

export class EventAPI extends BasePGSqlAPI {
  constructor(container, type, typeMap, config) {
    super(container, type, typeMap, config)
    this.log.info(
      `EventAPI Create  type: ${this.getTypeName()} config:${JSON.stringify(
        config
      )}`
    )
  }
}
