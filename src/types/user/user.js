import { BaseType, Permissions } from "@brownpapertickets/surya-gql-types"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"

export { Address } from "../common"

export class User extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "User",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        id: {
          type: ScalarTypes.ID,
          key: true,
          required: true,
          ownerId: true,
          input: false,
        },
        email: {
          type: "EmailScalar",
          index: true,
          unique: true,
          readPermission: Permissions.Personal,
        },
        userName: {
          type: ScalarTypes.String,
          index: true,
          unique: true,
        },
        firstName: {
          type: ScalarTypes.String,
        },
        lastName: {
          type: ScalarTypes.String,
        },
        createdDate: {
          type: ScalarTypes.DateTime,
        },
        updatedDate: {
          type: ScalarTypes.DateTime,
        },
        clientId: {
          type: ScalarTypes.Integer,
        },
        emailToken: {
          type: ScalarTypes.Object,
          objectType: "AuthToken",
          readPermission: Permissions.Personal,
        },
        address: {
          type: ScalarTypes.Object,
          objectType: "Address",
        },
      },
    }
  }
}

export class AuthToken extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "AuthToken",
      extends: null,
      instantiable: false,
      fields: {
        value: {
          type: ScalarTypes.String,
        },
        created: {
          type: ScalarTypes.DateTime,
        },
      },
    }
  }
}
