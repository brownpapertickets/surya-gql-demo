import { BaseMongoAPI } from "@brownpapertickets/surya-gql-data-mongodb"

export class UserAPI extends BaseMongoAPI {
  constructor(container, type, typeMap, config) {
    super(container, type, typeMap, config)
    this.log.info(
      `UserAPI create type: ${this.getTypeName()} config:${JSON.stringify(
        config
      )}`
    )
  }

  // getCollectionName() {
  //   return "demo-user"
  // }

  // getCollection() {
  //   return this.db.collections["User"]
  // }

  // async get(id, session) {
  //   const col = this.getCollection()
  //   let doc
  //   if (session) {
  //     doc = await col.findById(id).session(session)
  //   } else {
  //     doc = await col.findById(id)
  //   }

  //   if (doc) {
  //     return Object.create(this.type).loadDocument(doc)
  //   }
  //   return null
  // }
}
