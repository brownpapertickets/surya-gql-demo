export const UserResolvers = {
  Query: {
    users: async (_, args, context) => {
      return await context.managers
        .get("User")
        .query(args, context, { doAuth: true })
    },
    user: async (_, args, context) => {
      return await context.managers
        .get("User")
        .get(args, context, { doAuth: true })
    },
  },
  Mutation: {
    createUser: async (_, args, context) => {
      return await context.managers
        .get("User")
        .create(args.user, context, { doAuth: true })
    },
    updateUser: async (_, args, context) => {
      return await context.managers
        .get("User")
        .update(args.id, args.user, context, {
          doAuth: true,
        })
    },
    deleteUser: async (_, args, context) => {
      return await context.managers
        .get("User")
        .delete(args.id, context, { doAuth: true })
    },
  },
}
