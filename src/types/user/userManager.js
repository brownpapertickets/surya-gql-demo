import { BaseCRUDManager } from "@brownpapertickets/surya-gql-types"

const authorized = {
  get: { groups: ["admin"], owner: true },
  query: { groups: ["admin"] },
  create: { groups: ["admin"] },
  update: { groups: ["admin"], owner: true },
  delete: { groups: ["admin"] },
}

export class UserManager extends BaseCRUDManager {
  authorized() {
    return authorized
  }
}
