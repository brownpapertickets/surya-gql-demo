export const Manifest = {
  sysVersion: "1.0.0",
  typeVersion: "0.1.0",
  description: "MongoDB Based Users storage",
  types: ["User", "Address", "AuthToken"],
  queries: "UserQueries",
  resolvers: "UserResolvers",
  manager: "UserManager",
  data: {
    interface: "MongoDB",
    api: "UserAPI",
    config: { collection: "demo-user" },
  },
}
