export class MockLog {
  info(message) {
    console.info(`Info: ${message}`)
  }
  warn(message) {
    console.warn(`Warn: ${message}`)
  }

  error(message) {
    console.error(`Error: ${message}`)
  }
}
